package com.r8
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.Dataset

object Load {
  
  def main(args: Array[String]): Unit = {
    
    val input=args(0)
    
    
    val spark=SparkSession.builder().appName(args(1)).master("local[*]").enableHiveSupport().
        config("spark.sql.shuffle.partitions","50").getOrCreate()
       
    
    val sales=spark.read.option("header","true").option("inferschema","true").csv(input)
    
    val agg=sales.select("sto_no","syw","art_no","art_qty_sale").groupBy("sto_no","syw").agg(round(sum("art_qty_sale")).as("total_sales")).
    orderBy(desc("total_sales"))
    
    agg.show(100)
    
    
  }
  
}